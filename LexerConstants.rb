#!/usr/bin/env ruby
# Encoding: utf-8
# frozen_string_literal: true

# Helper file to hold constants for lexer tokens
DUMMY           = 0
ERROR           = 1
PHP_OPEN        = 2
PHP_CLOSE       = 3
VARIABLE        = 4
CONSTANT        = 5
BLOCK_OPEN      = 6
BLOCK_CLOSE     = 7
PARENT_OPEN     = 8
PARENT_CLOSE    = 9
CURLY_OPEN      = 10
CURLY_CLOSE     = 11
STRING_SINGLE   = 12
STRING_DOUBLE   = 13
STRING_BACKTICK = 14
NUMBER          = 15
OPERATOR        = 16
COMMA           = 17
COMMENT_SINGLE  = 18
COMMENT_MULTI   = 19
TEXT            = 20
EOF             = 21
