#!/usr/bin/env ruby
# Encoding: utf-8
# frozen_string_literal: true

# Class that contains all of the language lexing functions
class LexerFunctions
  def lex_text
    puts 'lex_text'
    method :lex_number
  end

  def lex_number
    puts 'lex_number'
    nil
  end
end
