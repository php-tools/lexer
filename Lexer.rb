#!/usr/bin/env ruby
# Encoding: utf-8
# frozen_string_literal: true

require_relative 'LexerFunctions.rb'

# Main class for the PHP lexer
class Lexer < LexerFunctions
  require 'thread'
  require_relative 'LexerConstants.rb'

  attr_accessor :input, :output
  attr_reader   :start, :pos

  def initialize(debug)
    puts 'New Lexer initialized' if debug
    @debug = debug
  end

  # Kickstart the lexer
  def start(filename, queue)
    @input = read_file(filename)
    @start = 0
    @pos   = 0
    @queue = queue
    lex method :lex_text
  end

  # Main lexer function loop
  def lex(state)
    Thread.new do
      state = state.call until state.nil?
    end
    emit nil
  end

  # Send lex token to parser queue
  def emit(type)
    # Put input[start..pos] into queue
    @queue.push type
  end

  # Get next char
  def nxt
    # Move pos right by one and return the char
  end

  # Ignore character
  def ignore
    @start = @pos
  end

  # Take a step back
  def backup
    @pos -= 1
  end

  # Look at the next char without consuming it
  def peek
    c = nxt
    backup
    c
  end

  # Try to read file to lex
  def read_file(filename)
    filename = File.expand_path filename
    @input   = File.read filename
    puts "Read #{filename}" if @debug
  end
end
